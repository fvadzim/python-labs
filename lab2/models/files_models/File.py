# -*- coding: utf-8 -*-
import os, logging
from bin.helpers.resol import get_resol


class File(object):
    def __init__(self, name, path=os.getcwd()):
        self.name = name
        self.id_name = name
        self.path = path
        self.full_path = os.path.join(path, name)
        self.R_OK, self.W_OK, self.X_OK = (os.access(self.full_path, os.R_OK),
                                           os.access(self.full_path, os.W_OK),
                                           os.access(self.full_path, os.X_OK))
        self.logger = logging.getLogger("application.file")

    def get_size(self):
        """To be implemented in descendants"""
        pass
