# -*- coding: utf-8 -*-
import os
from File import File


class JustFile(File):
    def __init__(self, filename, path=os.getcwd()):
        super(JustFile, self).__init__(filename, path)

    def get_size(self):
        return os.path.getsize(self.full_path)
