# -*- coding: utf-8 -*-
import os
from File import  File

class Directory(File):
    def __init__(self, dir_name='', path = os.getcwd()):
        super(Directory, self).__init__(dir_name, path)
        if self.R_OK : self.init_dirs()

    def init_dirs(self):
        self.dirs  = [d for d in os.listdir(self.full_path) if
                      (os.path.isdir(os.path.join(self.full_path, d))
                       and not os.path.islink(os.path.join(self.full_path, d)))]
        self.files = [f for f in os.listdir(self.full_path)
                      if (os.path.isfile(os.path.join(self.full_path, f))
                          and not os.path.islink(os.path.join(self. full_path, f)))]
        self.links = [l for l in os.listdir(self.full_path)
                      if os.path.islink(os.path.join(self.full_path, l))]

    @property
    def get_dirs(self):
        return self.dirs

    def get_size(self):
        res_size = 0
        for item in os.walk(self.full_path):
            for file in item[2]:
                try:
                    res_size += os.path.getsize(os.path.join(item[0], file))
                except:
                    print("error with file:  " + os.path.join(item[0], file))
        return res_size


if __name__ == "__main__":
    _dir_ = os.path.dirname(os.path.abspath(__file__))
    print os.path
