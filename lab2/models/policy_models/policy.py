# -*- coding: utf-8 -*-
from bin.helpers.resol import *
import datetime
from bin.dispose import Disposer
from bin.helpers.helpers import *
from config.config import Configuration
from crontab import CronTab
from bin.access import Accesser

class iCleanUpPolicy(object):
    def __init__(self):
        super(iCleanUpPolicy, self).__init__()

    def run(self):
        raise NotImplementedError


class MemoryLackPolicy(iCleanUpPolicy):
    def __init__(self,
                 trash_path,
                 info_path,
                 critical_size):
        super(MemoryLackPolicy, self).__init__()
        self.trash_path = trash_path
        self.info_path = info_path
        self.critical_size = critical_size

    def run(self):
        trash_files = iter(os.listdir(self.trash_path))
        cur_size = get_sys_freespace()
        dispose_list = []
        while cur_size < self.critical_size:
            try:
                inst = get_instance(self, os.path.join(self.trash_path, next(trash_files)))
                inst_size = inst.getSize()
                cur_size += inst_size
                dispose_list.extend([inst.full_path,
                                     os.path.join(self.info_path, inst.name + '.trashinfo')])
            except:
                return dispose_list
        return dispose_list


class OverflowCleanUpPolicy(iCleanUpPolicy):
    def __init__(
            self,
            trash_path,
            info_path,
            max_size):
        super(OverflowCleanUpPolicy, self).__init__()
        self.max_size = max_size
        self.trash_path = trash_path
        self.info_path = info_path

    def run(self):
        trash_files = iter(os.listdir(self.trash_path))
        cur_size = Directory.get_size(Directory(*os.path.split(self.trash_path)[::-1]))
        dispose_list = []
        while cur_size > self.max_size:
            inst = get_instance(self,
                                os.path.join(self.trash_path, next(trash_files)))
            inst_size = inst.getSize()
            cur_size -= inst_size
            dispose_list.append(inst.name)
        return dispose_list


class TimeCleanUpPolicy(iCleanUpPolicy):
    def __init__(self,
                 trash_path,
                 info_path,
                 days=7,
                 hours=0
                 ):
        super(TimeCleanUpPolicy, self).__init__()
        self.trash_path = trash_path
        self.info_path = info_path
        self.hours = hours
        self.days = days
        self.config_parser = Configuration()

    def run(self):
        trash_files = iter(os.listdir(self.trash_path))
        dispose_list, thashold_time = (
            [],
            (datetime.datetime.now() - datetime.timedelta(
                days=self.days,
                hours=self.hours)).isoformat()[:-7])
        for inst_name in trash_files:
            trash_info_file = os.path.join(
                self.info_path,
                inst_name + '.trashinfo')
            self.config_parser.import_settings(trash_info_file)
            time = self.config_parser.settings['Trash Info']['DeletionDate']
            if time < thashold_time:
                dispose_list.extend([
                    os.path.join(self.trash_path, inst_name),
                    os.path.join(self.info_path, '{}.trashinfo'.format(inst_name))
                ])
        return dispose_list


class AskBeforeCleanUpPolicy(iCleanUpPolicy):
    def __init__(self,
                 policy):
        super(AskBeforeCleanUpPolicy, self).__init__()
        self.policy = policy

    def run(self):
        if clean_up_resol():
            return self.policy.run()


class CountCleanUpPolicy(iCleanUpPolicy):
    def __init__(self,
                 trash_path,
                 info_path,
                 max_count):
        super(CountCleanUpPolicy, self).__init__()
        self.trash_path = trash_path
        self.info_path = info_path
        self.max_count = max_count

    def run(self):
        all_insts = os.listdir(self.trash_path)
        res_inst = all_insts[:len(all_insts) - self.max_count]
        return ([os.path.join(self.trash_path, name) for name in res_inst] +
                [os.path.join(self.info_path, name + '.trashinfo') for name in res_inst])


class OfflineCleanUpPolicy(iCleanUpPolicy):
    def __init__(
            self,
            days,
            hours,
            minutes):
        super(OfflineCleanUpPolicy, self).__init__()
        self.days = days
        self.minutes = minutes
        self.hours = hours

    def run(self):
        tab = CronTab(user= 'citizen4')
        cmd = 'offline_clean'
        cron_job = tab.new(cmd, comment='offline_clean')
        cron_job.minutes.every(self.days * 24 * 60 +
                               self.hours * 60 +
                               self.minutes)
        cron_job.enable()
        tab.write()
    #def delete(self):
    #    tab = CronTab(user='citizen4')
    #    tab.remove_all(command='offline_clean_up')
    #    tab.write()

class iConflictNamePolicy(object):
    def __init__(self):
        super(iConflictNamePolicy, self).__init__()

    def run(self, file):
        raise NotImplementedError


class AskRenamePolicy(iConflictNamePolicy):
    def __init__(self):
        super(AskRenamePolicy, self).__init__()

    def run(self, file):
        ans = change_name(file.name)
        if ans == 'c':
            return ''
        elif ans == 'rn':
            return file.name + '(' + datetime.datetime.now().isoformat()[:-7] + ')'
        elif ans == 'rp':
            return file.name


class SilentRenamePolicy(iConflictNamePolicy):
    def __init__(self):
        super(SilentRenamePolicy, self).__init__()

    def run(self, file):
        return file.name + '(' + datetime.datetime.now().isoformat()[:-7] + ')'


class DelCompetitorPolicy(iConflictNamePolicy):
    def __init__(self):
        super(DelCompetitorPolicy, self).__init__()
        self.disposer = Disposer()

    def run(self, file, dst=None):
        oldfile_name = os.path.join(dst, file.name)
        self.disposer.dispose_by_name([oldfile_name])
        return file.name


class iAccessPolicy(object):
    def __init__(self):
        super(iAccessPolicy, self).__init__()

    def run(self, inst):
        return NotImplementedError


class ForceAccessPolicy(iAccessPolicy):
    def __init__(self, args):
        super(ForceAccessPolicy, self).__init__()
        self.accesser = Accesser(args)

    def run(self, inst):
        return self.accesser.get_access(inst)


class InteractiveAccessPolicy(iAccessPolicy):
    def __init__(self, args):
        super(iAccessPolicy, self).__init__()
        self.accesser = Accesser(args)

    def run(self, inst):
        return self.accesser.interactive_get_access(inst)


class PolicyAggregator(object):
    def __init__(self, policy_list):
        super(PolicyAggregator, self).__init__()
        if not isinstance(policy_list, list):
            raise Exception('policy_list must be of list type')
        self.policy_list = policy_list

    def run(self):
        dispose_list = set()
        for policy in self.policy_list:
            dispose_list.update(policy.run())
        return dispose_list

