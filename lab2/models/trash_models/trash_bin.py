# -*- coding: utf-8 -*-
from models.policy_models.policy import *
from bin.helpers.helpers import get_sys_freespace
from os.path import join,abspath
import logging

class TrashBin(object):
    def __init__(self,
                 args,
                 path,
                 max_size,
                 critical_size_left):
        print 'trashbin path {0}'.format(args)
        print 'trashbin args {0}'.format(path)

        print 'os.environ["HOME"] {0}'.format(os.environ['HOME'])
        self.full_path = os.environ["BIN"] = join(os.environ['HOME'], path)
        if not os.path.exists(self.full_path):
            print '! os.path.exists(abspath(path)) {0}'.format(self.full_path)
            os.mkdir(self.full_path)
        self.critical_size_left = critical_size_left
        self.max_size = max_size
        self.args = (self.dry_run,
                     self.force,
                     self.interactive,
                     self.silent) = args
        self.files_path = join(self.full_path, 'files')
        if not exists(self.files_path):
            if not self.dry_run:
                os.mkdir(self.files_path)
        self.info_path = join(self.full_path, 'info')
        if not exists(self.info_path):
            if not self.dry_run:
                os.mkdir(self.info_path)
        self.parser = Configuration()
        self.logger = logging.getLogger("application.trashBin")
        self.dry_run, self.force, self.interact, self.silent = args

    """
           creates a new symlink on content of old one.unlink old symlink.
           unfortunately "os" module author worked on does not support os.lchmod
           link_name : full_path to a link
    """

    def get_free_size(self):
        """Returns size in bytes"""
        return min(get_sys_freespace(), self.max_size-self.get_size())

    def get_size(self):
        """Returns size in bytes"""
        path, name = os.path.split(self.full_path)
        return Directory(name, path).get_size()

    def is_free_to_get_file(self, file):
        return self.get_free_size() > file.get_size()

    def create_info(self, file):
        with open(join(self.info_path, file.id_name+'.trashinfo'),'w') as info_file:
            info_file.writelines(['[Trash Info]'+'\n',
                                  'Path={full_path}'.format(
                                      full_path=abspath(join(file.path, file.name)))+'\n',
                                  'DeletionDate={date}'.format(
                                          date=datetime.datetime.now().isoformat()[:-7])])
