# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
from os.path import join, dirname
from bin.main_rm import  main_rm

setup(
    name='unique_name',
    version='1.0',
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
    data_files=[('config', ['config/log_conf.json'])],
    packages=find_packages(),
    entry_points={
        'console_scripts':
            ['offline_clean = bin.main_offline:offline_clean_up',


             'rs2 = bin.main_rs:main_rs',


             'rm2 = bin.main_rm:main_rm',


              'setconf = bin.main_config:main_config'
             ]
        },
    author='Vadzim Filipovich',
    author_email='fvadzim@gmail.com'
    )
