import  configobj
from configobj import ConfigObj, ConfigObjError
import json
import logging
import logging.config
import os

class Configuration(object):
    def __init__(self, file_name=None, logger_dict_config=None):
        super(Configuration, self).__init__()
        logging.basicConfig(
            format="%(asctime)-15s %(levelname)s: %(message)s at func: `%(funcName)s`",
            datefmt="%Y-%m-%d %H:%M:%S",
            level = "ERROR"
        )
        self.logger = logging.getLogger()
        if file_name:
            try:
                self.import_settings(file_name)

            except ConfigObjError:
                self.import_json_settings(file_name)
            except:
                self.logger.error("Failed to read config in %s " %
                    'Configuration.import_settings'
                )
                exit("exit code : 5  fail to read config file")
        else:
            self.settings = None

    def import_json_settings(self, file_name):
        with open(file_name, 'rt') as jsonConfig:
            self.settings = (json.loads(jsonConfig.read()))
        self.filename = self.settings.filename = file_name

    def import_settings(self, file_name):
        try:
            self.settings = ConfigObj(file_name)
        except configobj.NestingError:
            self.logger.error("""Error while reading config.
                             There is field without option found in
                             configurational file""")
            raise Exception
        except configobj.RepeatSectionError:
            self.logger.error("""Error while reading config.
            There are duplecated sections in
            configurational file""")
            raise Exception
        except configobj.ParseError:
            self.logger.error("""Error while parsing config.
            There is no required section in
            configurational file""")
            raise Exception
        except configobj.DuplicateError:
            self.logger.error("""Error while parsing config.
            There is no duplicated section in
            configurational file""")
            raise Exception
        except configobj.ConfigObjError:
            raise Exception("Error while reading configurational file")
        self.filename = self.settings.filename =os.path.abspath( file_name)

    def change_conf_param(self, param_path, value):
        if not (isinstance(param_path, list) or isinstance(param_path, tuple)):
            raise Exception('fuck off')
        lol = self.settings
        for param in param_path[:-1]:
            try:
                lol = lol[param]
            except:
                self.logger.error('KeyError while getting parmam %s' % param)
        try:
            lol[param_path[-1]] = value
        except:
            self.logger.error('KeyError while setting parmam %s' % param_path[-1])
        self.settings.write()

