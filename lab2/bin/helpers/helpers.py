import os
from os.path import  exists, isdir, islink, split,abspath
from models.files_models.Directory import Directory
from models.files_models.JustFile import JustFile
from models.files_models.File import File
from bin.helpers.resol import get_resol

def get_instance(self, abs_path):
    if not exists(abs_path):
        if not self.silent:
            self.logger.error("file %s does not exist" % abs_path)
        exit(1)
    name, path = (split(abspath(abs_path))[::-1])
    if islink(abs_path):
        return File(name, path)
    elif isdir(abs_path):
        return Directory(name, path)
    else:
        return JustFile(name, path)

def get_sys_freespace(path = os.environ['HOME']):
    s = os.statvfs(path)
    return s.f_bsize*s.f_bavail


def get_access(inst):
        inst.logger.warning('Getting "%s" file  access permissions' % inst.name)
        try:
            os.chmod(inst.full_path, 0777)
        except:
            inst.logger.error("""Permissions of %s can't be changed.
                                 Try run this command as superuser""" %
                inst.full_path)
            exit("exit code 1")
        inst.logger.info("Permission accessed")


def silent_access(inst):
        inst.get_access()
        return True

def interactive_get_access(inst):
        if inst.W_OK and inst.R_OK:
            return True
        else:
            if not get_resol(appeal="\n File {fileName} is protected."
                                   "Delete it anyway? :\n ".format(
                fileName=inst.full_path)
            ):
                return False
            inst.get_access()
            return True