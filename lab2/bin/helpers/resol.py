
def get_resol(appeal = """File is protected.Delete it anyway?
                       (in oreder to delete all click all(a/all/) ) : """,
             pos=["y", "yes", "yeah"],
             all=["all","a"],
             neg=["n", "no", "nope"],
             ignoreCase=True,
             count = 0,
             maxCount = 4):
    answer = raw_input(appeal)
    if ignoreCase:
        pos.extend([st.upper() for st in pos])
        neg.extend([st.upper() for st in neg])
        all.extend([st.upper() for st in all])
        answer = answer.upper()

    if answer in all:
        return 'a'
    elif answer in pos:
        return 'unique'
    elif answer in neg or count > maxCount:
        return False
    else:
        get_resol(appeal, pos, all, neg, ignoreCase, count + 1)


def change_name(fileName,
                appeal = """There already is a folder named {name}.
                Replace older one(older one will be disposed)? Replace/ rep/ rp
                Rename newer folder with {name}(delete time(ISO))? Rename/ ren/ rn ,
                Rename all the left all/ALL/a,
                Cancel? Cancel/ cnl/ c /n : """,
                cnl=["cancel", "cnl", "c", "n"],
                rep=["rep", "replace", "rp"],
                ren=["ren", "ren", "rn"],
                all = ["all","a"],
                ignoreCase=True,
                count=0,
                maxCount=4):
    answer = raw_input(appeal.format(name = fileName))
    if ignoreCase:
        cnl.extend([st.upper() for st in cnl])
        rep.extend([st.upper() for st in rep])
        ren.extend([st.upper() for st in ren])
        all.extend([st.upper() for st in all])
        answer = answer.upper()
    if answer in cnl or count > 4:
        return 'c'
    elif answer in rep:
        return 'rp'
    elif answer in ren:
        return 'rn'
    elif answer in all:
        return 'all'
    else:
        change_name(fileName, appeal, cnl, rep, ren, all, ignoreCase, count + 1, maxCount)


def clean_up_resol():
    return get_resol(
             appeal="Trash Bin is overflown.Clean it up? : ",
             pos=["y", "yes", "yeah"],
             all=["all","a"],
             neg=["n", "no", "nope"],
             ignoreCase=True,
             count=0,
             maxCount=4)
