# -*- coding: utf-8 -*-
import argparse
import ConfigParser
import logging
import logging.config
import json
import os
from os.path import join, abspath, split


def get_args_parser():
    args_parser = argparse.ArgumentParser(prog='unique_name',
                                          description='parse the arguments of rm2')
    #args_parser.add_argument('action', type=str, choices=['rm2', 'rs2'],
    #                         help='action to perform')
    args_parser.add_argument('files', type=str,
                             nargs='*',
                             help='list of files to delete or restore')
    args_parser.add_argument('-reg', '--regex',
                             type=str,
                             nargs='+',
                             required = False,
                             dest='regex',
                             help='list of files to delete or restore using regular expr')
    args_parser.add_argument("-f", "--force",
                             action="store_true",
                             dest="force",
                             help="delete all the files without resolutions")
    args_parser.add_argument("-i", "--interactive",
                             action="store_true",
                             dest="interactive",
                             help="i don't know")
    args_parser.add_argument('-dr', '--dry_run',
                             dest='dry_run',
                             action='store_true',
                             help='dry-run help')
    args_parser.add_argument('-s', '--silent',
                             action='store_true',
                             dest='silent',
                             help='--silent help')
    args_parser.add_argument('-v', '--value', type=str,
                             dest='value',
                             help='value help')
    args_parser.add_argument('-r', '--rec',
                             action='store_true',
                             dest='recursive',
                             required=False)
    args_parser.add_argument('-b', '--bin', type=str,
                             dest='bin',
                             required=False)
    args_parser.add_argument('-p', '--politics', help='politics help')

    args_parser.add_argument('-c', '--config', type=str,
                             help='path to configuratational file to be used')
    return args_parser

def get_default_conf_parser():
    args_parser = argparse.ArgumentParser(prog='unique_name',
                                          description='parse the arguments of unique_name')

    args_parser.add_argument('param_path', type=str,
                             nargs='+',
                             help='nested path to config param')
    args_parser.add_argument('-v', '--value', type=str,
                             dest = 'value',
                             help = 'a value to set')
    return args_parser



def get_config_parser(config_path=None):
    config = ConfigParser.SafeConfigParser(allow_no_value=False)  # path ?

    if config_path:
        try:
            config.readfp(open(config_path))
        except ConfigParser.NoSectionError:
            raise Exception("""Error while reading config.
                            There is no required section in
                            configurational file""")

        except ConfigParser.DuplicateSectionError:
            raise Exception("""Error while reading config.
                             There are duplecated sections in
                             configurational file""")

        except ConfigParser.NoOptionError:
            raise Exception("""Error while reading config.
                             There is field without option found in
                             configurational file""")
        except ConfigParser.ParsingError:
            raise Exception("""Error while parsing configurational file""")

        except ConfigParser.Error:
            raise Exception("Error while reading configurational file")

    return config


def get_logger_config(app, logger_name, log_path=None):
    if log_path is None:
        log_path = abspath(join(abspath(split(app.path)[0]),
                                'config/log_conf.json'))
    with open(log_path, 'rt') as f:
        config = json.load(f)
        if not config['handlers']['file_handler']['filename']:
            report = abspath(join(os.environ["HOME"],'.rm2', 'report'))
            #report = abspath(join(abspath(split(app.path)[0]), 'etc/report'))
            #os.chmod(report, 0777)
            config['handlers']['file_handler']['filename'] = report
        logging.config.dictConfig(config)
        return logging.getLogger(logger_name)