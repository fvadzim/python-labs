import os, sys
from config.config import Configuration
from bin.helpers.parse import get_default_conf_parser, get_logger_config
import logging
import logging.config

def main_config():
    argsParser = get_default_conf_parser()
    try:
        namespace = argsParser.parse_args()
    except:
        raise Exception("Invalid arguments")

    config_path = os.path.abspath(
        os.path.join(
            os.environ["HOME"], '.rm2', 'configuration')
    )
    config = Configuration(config_path)
    config.change_conf_param(namespace.param_path, namespace.value)
    exit(0)


if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(__file__)))
    main_config()
