# -*- coding: utf-8 -*-
from itertools import chain
from fnmatch import fnmatch
import re
from os.path import join
from models.trash_models.trash_bin import TrashBin
from bin.helpers.parse import get_logger_config
from bin.remove import Remover
from models.policy_models.policy import *

class Application(object):
    def __init__(self, args, path, user_bin_path = None, config_path=None):
        super(Application, self).__init__()

        self.path = path
        if config_path is None:
            self.config_path = abspath(
                join(os.environ['HOME'], '.rm2','configuration'))
        else:

            self.config_path = abspath(config_path)
        print self.config_path
        self.config = Configuration(file_name=self.config_path)
        self.logger = get_logger_config(self, logger_name="application")
        self.args = (self.dry_run,
                     self.force,
                     self.interactive,
                     self.silent) = args
        if self.force and self.interactive:
            self.logger.error('incompatibe args : -f and -i')
            exit(1488)

        print 'confgig path to trahs(application 37) {0}'.format(self.config.settings['Trash']['pathToTrash'])
        self.trash_bin = TrashBin(
            self.args,
            user_bin_path if user_bin_path
            else join(os.environ['HOME'], self.config.settings['Trash']['pathToTrash']),
            self.config.settings['Size']['maxSize'],
            self.config.settings['Size']['criticalSize']
        )
        self.remover = Remover(
                 self.args,
                 float(self.config.settings['Remove']['maxSize/ram']),
                 int(self.config.settings['Remove']['blockSize']),
                 {'conflict_name':
                      self.config.settings['Policies']['conflict_name'],
                  'access':
                      self.config.settings['Policies']['access']},
                 info_path=self.trash_bin.info_path,
                 trash_path=self.trash_bin.files_path
                 )
        self.disposer = self.remover.disposer

        self.clean_up_policy  = self.get_clean_up_policy_from_config(
            self.config.settings['Policies']['clean_up'])
        if self.interactive and not self.force:
            self.clean_up_policy = AskBeforeCleanUpPolicy(
                self.clean_up_policy)
        if (self.config.settings['Policies']['offline'] == 'True' and
            self.config.settings['Policies']['offline_set']['set'] == 'False'):
            days , hours, minutes = (int(self.config.settings['Policies']['delay']['days']),
                                     int(self.config.settings['Policies']['delay']['hours']),
                                     int(self.config.settings['Policies']['delay']['minutes']))
            OfflineCleanUpPolicy(days, hours, minutes).run()
            self.config.change_conf_param(['Policies', 'offline_set', 'set'],'True')


    def rm(self, *files):
        if not self.silent:
            self.logger.debug("files to remove : %s" % str(files))
        for file_name in files:
            del_inst = get_instance(self, os.path.abspath(
                os.path.join(os.getcwd(), file_name)))
            self.remover.remove(del_inst, self.trash_bin.files_path)
            self.trash_bin.create_info(del_inst)

    def rs(self, *files):
        if not self.silent:
            self.logger.debug('files to restore : %s' % str(files))
        if not files:
            files =  os.listdir(self.trash_bin.files_path)
        for file_name in files:
            try:
                self.remover.restore_inst(get_instance(
                    self, os.path.join(self.trash_bin.files_path, file_name)))
            except:
                pass
    def rm_by_reg(self,
                  cur_dir = os.getcwd(),
                  relative_path ='',
                  patterns = None,
                  rec = False):
        if patterns is None:
            return
        if not self.silent:
            self.logger.debug("remove patterns : %s" % str(patterns))
        for pattern in patterns:
            for inst_name in os.listdir(cur_dir):
                full_name = os.path.join(cur_dir, inst_name)
                try:
                    if re.search(pattern, full_name):
                        inst_path = os.path.join(relative_path, inst_name)
                        self.rm(inst_path)
                except:
                    pass
                if os.path.isdir(full_name) and rec :
                    self.rm_by_reg(os.path.join(cur_dir, inst_name),
                                   os.path.join(relative_path, inst_name),
                                   patterns, rec)

    def rs_by_reg(self, *patterns):
        if not self.silent:
            self.logger.debug("restore patterns : %s " % str(patterns))
        for pattern in patterns:
            for inst_name in os.listdir(self.trash_bin.files_path):
                try:
                    if re.search(pattern, inst_name):
                        self.rs(inst_name)
                except:
                    pass

    def get_clean_up_policy_from_config(self, policy_name):
        if (isinstance(policy_name, list) or
            isinstance(policy_name, tuple) or
            isinstance(policy_name, set)):
                return PolicyAggregator(
                    [self.get_clean_up_policy_from_config(policy)
                     for policy in policy_name])
        trash_path = self.trash_bin.files_path
        info_path = self.trash_bin.info_path
        max_count = int(self.config.settings['Trash']['maxCount'])
        max_size = int(self.config.settings['Size']['maxSize'])**1024*2
        critical_size = int(self.config.settings['Size']['criticalSize'])**1024*2
        days = int(self.config.settings['Policies']['delay']['days'])
        hours = int(self.config.settings['Policies']['delay']['hours'])
        minutes = int(self.config.settings['Policies']['delay']['minutes'])
        if policy_name == 'count':
            return CountCleanUpPolicy(trash_path,
                                      info_path,
                                      max_count)
        elif policy_name == 'time':
            return TimeCleanUpPolicy(trash_path,
                                     info_path,
                                     days,
                                     hours)
        elif policy_name == 'offline':
            return OfflineCleanUpPolicy(days,
                                        hours,
                                        minutes)
        elif policy_name == 'overflow':
            return OverflowCleanUpPolicy(trash_path,
                                         info_path,
                                         max_size)
        elif policy_name == 'memoryLack':
            return MemoryLackPolicy(trash_path,
                                    info_path,
                                    critical_size)