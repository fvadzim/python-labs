# -*- coding: utf-8 -*-
import sys
import os
#sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from bin.application import Application
from bin.dispose import Disposer


def offline_clean_up():
    setup_dir = os.path.dirname(os.path.abspath(__file__))
    args = [False,False,False,True]
    application = Application(args, setup_dir)
    files_to_dispose = application.clean_up_policy.run()
    application.disposer.dispose_by_name(list(files_to_dispose))

