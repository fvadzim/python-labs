# -*- coding: utf-8 -*-
import logging
import os
from bin.helpers.resol import *


class Accesser(object):
    def __init__(self, args):
        super(Accesser, self).__init__()
        self.args = (self.dry_run,
                     self.force,
                     self.interactive,
                     self.silent) = args
        self.logger = logging.getLogger('application.accesser')

    def get_access(self, inst):
        if type(inst).__name__ in ['File', 'JustFile']:
            return self.force_access_to_file(inst)
        else:
            return self.force_access_to_dir(inst)


    def interactive_get_access(self, inst):
        if inst.W_OK and inst.R_OK:
            return True, 'unique'
        else:
            res =  get_resol(appeal="""\n File {fileName} is protected.
                Delete it anyway?"
                                   (pritnt a/all in order to let the same for the all filse left) :\n """.format(
                fileName=inst.full_path)
            )
            if not res:
                return False
            else:
                return self.get_access(inst), res




    def force_access_to_file(self, inst):
        try:
            os.chmod(inst.full_path, 0777)
        except:
            if not self.silent:
                self.logger.error("""Permissions of %s can't be changed.
                Try run this command as superuser""" %
                inst.full_path)
                exit("1")
        if not self.silent:
            self.logger.info("Permission accessed")
        return True

    def force_access_to_dir(self, dir):
        if self.force_access_to_file(dir):
            dir.init_dirs()
            return True
        else:
            return False
