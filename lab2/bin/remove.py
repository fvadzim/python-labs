# -*- coding: utf-8 -*-
import logging
import psutil as ps
from os.path import *
import shutil
from models.policy_models.policy import *


class Remover(object):
    def __init__(self,
                 args,
                 max_ram_percent,
                 block_size,
                 policies,
                 info_path=None,
                 trash_path=None):
        super(Remover, self).__init__()
        self.args = (self.dry_run,
                     self.force,
                     self.interactive,
                     self.silent) = args
        self.disposer = Disposer(self.args)
        self.parser = Configuration()
        self.dry_run, self.force, self.interact, self.silent = args
        self.max_ram = float(max_ram_percent)*int(ps.virtual_memory().total)
        self.block_size = 1024**2*block_size
        if info_path:
            self.info_path = info_path
        else:
            self.info_path = join(os.environ["BIN"], 'info')
        if trash_path:
            self.trash_path = trash_path
        self.logger = logging.getLogger('application.remover')

        if ((policies['conflict_name'] == 'ask' or self.interactive)
                and not  self.force):
            self.conflict_name_policy = AskRenamePolicy()
        elif policies['conflict_name'] == 'silent':
            self.conflict_name_policy = SilentRenamePolicy()
        elif policies['conflict_name'] == 'del_competitor':
            self.conflict_name_policy = AskRenamePolicy()

        if ((policies['access'] == 'ask' or self.interactive)
        and not self.force):
            self.access_policy = InteractiveAccessPolicy(self.args)
        else:
            self.access_policy = ForceAccessPolicy(self.args)



    def remove(self, inst, relative_path):
        if inst.get_size() > get_sys_freespace():
            if not self.silent:
                self.logger.error("Not enough space for removing %s" %
                                  (inst.full_path))
            exit(228)

        if type(inst).__name__ == 'File':
            self.remove_link(inst, relative_path)
        #elif isinstance(inst, Directory):
        if type(inst).__name__ == 'Directory':
            self.remove_dir(inst, relative_path)
        else:
            self.remove_file(inst, relative_path)

    def remove_dir(self, folder,
                   relative_path=None):
        """
        :param folder: Directory class object
        :param relative_path: relative_path (path according to TrashBin)
        :return: True if directory_path can be deleted,returns False if not
        """
        if not self.silent:
            self.logger.info("Removing folder %s" % folder.full_path)
        access = self.access_policy.run(folder)

        if not access:
            return False
        

        if not isinstance(access, bool):
            if access[1]=='a':
                self.access_policy = ForceAccessPolicy(self.args)
        if relative_path is None:
            relative_path = self.trash_path

        try:
            folder.id_name = self.get_id_name(folder,
                                              dst=folder.original_path)
        except:
            folder.id_name = self.get_id_name(folder)


        if not folder.id_name:
            return
        if not self.dry_run:
            os.mkdir(join(relative_path, folder.id_name), 0777)
        path_to_descendents = join(relative_path, folder.id_name)
        delete_resolutions = []

        for dir_name in folder.dirs:
            sub_dir = Directory(dir_name, folder.full_path)
            delete_resolutions.append(self.remove_dir(sub_dir,
                                                      path_to_descendents))

        for file_name in folder.files:
            sub_file = JustFile(file_name, folder.full_path)
            delete_resolutions.append(self.remove_file(sub_file,
                                                       path_to_descendents))

        for link_name in folder.links:
            sub_link = File(link_name, folder.full_path)
            self.remove_link(sub_link, path_to_descendents)

        """  if there was an False at the bottom of rec return False  """
        if False in delete_resolutions:
            return False
        else:
            self.disposer.dispose_dir(folder)
            if not self.silent:
                self.remove_log(folder.full_path, relative_path)
            return True

    def remove_file(self, file, relative_path=None):
        if not self.silent:
            self.logger.debug("Removing file %s" % file.full_path)
        access = self.access_policy.run(file)
        if not access:
            if not self.silent:
                self.logger.debug("Permission denied. folder %s" % file.full_path)
            return False
        if not isinstance(access, bool):
            if access[1]=='a':
                self.access_policy = ForceAccessPolicy(self.args)

        if relative_path is None:
            relative_path = self.trash_path
            file.id_name = self.get_id_name(file)
            if not file.id_name:
                return

        if file.get_size() <= self.max_ram:
            if not self.dry_run:
                shutil.copyfile(file.full_path, join(relative_path, file.id_name))
        else:
            with open(file.full_path) as copyFile:
                file_to_path = join(relative_path, file.id_name)
                file_to = open(file_to_path, 'wb')
                while True:
                    bytes_add = copyFile.read(
                        1024**2*self.block_size)
                    if not bytes_add:
                        break
                    file_to.write(bytes_add)
        self.disposer.dispose_file(file)
        if not self.silent:
            self.remove_log(file.full_path, relative_path)
        return True

    """
           creates a new symlink on content of old one.unlink old symlink.
           unfortunately "os" module author worked on does not support os.lchmod
           link_name : full_path to a link
    """

    def remove_link(self, link,
                    relative_path=None):
        """Check if there are deleted files with such names"""
        self.logger.debug("Removing link %s" % link.full_path)
        if relative_path is None:
            link.id_name = self.get_id_name(link)
            if not link.id_name:
                return
            relative_path = self.trash_path
            if not self.dry_run:
                os.symlink(abspath(os.readlink(link.full_path)),
                           join(relative_path, link.id_name))
        self.disposer.dispose_link(link)
        if not self.silent:
            self.remove_log(link.full_path, relative_path)
        return True

    def remove_from_trash(self, inst):
        self.remove(inst, inst.original_path)

    def name_duplicated(self, name, dst=None):
        if dst is None:
            dst = self.trash_path
        return exists(join(dst, name))

    def get_id_name(self, file, dst=None):
        if self.name_duplicated(file.name, dst):
            if isinstance(self.conflict_name_policy, DelCompetitorPolicy):
                return self.conflict_name_policy.run(file, dst)
            else:
                return self.conflict_name_policy.run(file)
        else:
            return file.name

    def restore_inst(self, inst):
        try:
            if not self.silent:
                self.logger.debug('reading %s metadata' % inst.full_path)
            self.parser.import_settings(abspath(join(self.info_path, inst.name + '.trashinfo')))
            if not self.silent:
                self.logger.info('file original path %s ' %
                                 self.parser.settings['Trash Info']['Path'])
        except:
            if not self.silent:
                self.logger.debug("""
                {} restore path not found.
                Check the name is correct
                """.format(inst.full_path))
            exit("exit code 1")
        inst.original_path, inst.originalName = split(
            self.parser.settings['Trash Info']['Path'])
        inst.trashName = inst.name
        inst.name = inst.id_name = self.get_id_name(inst, dst=inst.original_path)
        if not os.path.exists(inst.original_path):
            if not self.dry_run:
                os.makedirs(inst.original_path)
        if not inst.name:
            return
        self.remove_from_trash(inst)
        self.disposer.dispose_by_name([abspath(join(self.info_path,
                                                    inst.trashName + '.trashinfo'))])
        if not self.silent:
            self.logger.info("Successfully restored to %s " %
                             join(inst.original_path, inst.id_name))

    def remove_log(self, frm, to):
        self.logger.debug("""Successfully removed from : %s
                            to : %s """ % (frm, to))
