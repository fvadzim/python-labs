#-*- coding: utf-8 -*-
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from bin.application import  Application
from bin.helpers.parse import get_args_parser


def main_rs():
    setupDir = os.path.dirname(os.path.abspath(__file__))
    argsParser = get_args_parser()
    try:
        namespace = argsParser.parse_args()
    except:
        raise Exception("Invalid arguments")
    # starts an application from a directory where was intalled
    args = (namespace.dry_run,
            namespace.force,
            namespace.interactive,
            namespace.silent)

    application = Application(args,
                              setupDir,
                              namespace.bin,
                              namespace.config)
    application.rs(*namespace.files)
    if namespace.regex:
        application.rs_by_reg(*namespace.regex)
    exit("exit code 0")


if __name__ == "__main__":
    main_rs()