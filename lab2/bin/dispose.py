# -*- coding: utf-8 -*-
import logging, sys
from bin.helpers.helpers import get_instance
import shutil
import os
from bin.access import Accesser

class Disposer(object):

    def __init__(self, args):
        super(Disposer,self).__init__()
        self.args = (self.dry_run,
                     self.force,
                     self.interactive,
                     self.silent) = args
        self.logger = logging.getLogger("application.remover.disposer")
        self.accesser = Accesser(self.args)
        if self.interactive:
            self.access_policy = self.accesser.interactive_get_access
        else:
            self.access_policy = self.accesser.get_access

    def dispose_by_name(self, file_names):

        if not (isinstance(file_names, list) or isinstance(file_names, tuple)
        or isinstance(file_names, set)):
            if not self.silent:
                self.logger.critical("file_names : %s " % file_names)
                self.logger.error( """dispose_by_name's func 'file_names' arg
                                    must be of iterable(but not str) type""")
            exit(2)
        for file_name in file_names:
            del_inst = get_instance(self, file_name)
            if type(del_inst).__name__ == 'File':
                self.dispose_link(del_inst)
            elif type(del_inst).__name__ == 'Directory':
                self.dispose_dir(del_inst)
            else : self.dispose_file(del_inst)
            if not self.silent:
                self.logger.info("file %s succesfuly disposed" % del_inst.full_path)

    def dispose(self, *del_insts):
        for del_inst in del_insts:
            if type(del_inst).__name__ == 'File':
                self.dispose_link(del_inst)
            elif type(del_inst).__name__ == 'Directory':
                self.dispose_dir(del_inst)
            else:
                self.dispose_file(del_inst)
            if not self.silent:
                self.logger.info("%s %s succesfuly disposed" % (
                    type(del_inst).__name__, del_inst.full_path))

    def dispose_link(self, link):
        if not self.dry_run:
            os.unlink(link.full_path)
        if not self.silent:
            self.logger.info("Link %s successfully disposed" % link.full_path)

    def dispose_file(self, file):
        access = self.access_policy(file)
        if not access:
            if not self.silent:
                self.logger.error("""Wrights to delete file :
                                    %s weren't accessed.
                                    Try to use superuser mode""" %
                                    file.full_path)
            sys.exit(3)
        if not isinstance(access, bool):
            if access[1]=='a':
                self.access_policy = self.accesser.get_access
        if not self.dry_run:
            os.remove(file.full_path)
        if not self.silent:
            self.logger.info("Link %s successfully disposed" % file.full_path)


    def dispose_dir(self, folder):
        access = self.access_policy(folder)
        if not access:
            if not self.silent:
                self.logger.error("""Wrights to delete file : %s
                weren't accessed.Try to use superuser mode""" % folder.full_path)
                sys.exit(3)
        if not isinstance(access, bool):
            if access[1]=='a':
                self.access_policy = self.accesser.get_access
        if not self.dry_run:
            shutil.rmtree(folder.full_path)
        if not self.silent:
            self.logger.info("Link %s successfully disposed" % folder.full_path)
